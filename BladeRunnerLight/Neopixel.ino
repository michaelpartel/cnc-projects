#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define PIN 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//Adafruit_NeoPixel strip = Adafruit_NeoPixel(24, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(30, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

//Setup upon power on
// Initialize board LED use
// Start Neopixel strip at brightness 50
// Clear all color from the strip just in case any residual remains
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  strip.setBrightness(50);

  strip.fill(strip.Color(0, 0, 0, strip.gamma8(50)));
}

void loop() {

  // Initalize Color and Brightness variables
  uint32_t R = strip.Color(255, 0, 0);
  uint32_t G = strip.Color(0, 255, 0);
  uint32_t B = strip.Color(0, 0, 255);
  // Triplet for White instead of Quadruplet since the Quad doesn't seem to work
  uint32_t W = strip.Color(255, 255, 255);
  uint32_t NA = strip.Color(0, 0, 0);
  uint8_t L = 200;
  uint8_t laddOnDelay = 10000;
  uint8_t textOnDelay = 20000;

  //Begin loop with heartbeat just in case you messed up
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW

  // Initialize strip as Black / Off for the loop. Just in case. Strip infrequently retains info.
  for (int thisPin = 0; thisPin < strip.numPixels(); thisPin++) {
    setColorAndBrightness(thisPin, 0, 0, 0, L);
  }

  // Set first panel's color to Green for Ladd Company Logo
  // Loop through pin 0 - 8 and set brightness to slowly "breathe" up
  for (int t1 = 0; t1 <= L; t1++) {
    for (int thisPin = 0; thisPin < 9; thisPin++) {
      setColorAndBrightness(thisPin, 0, 255, 0, t1);
    }
    strip.show();                // Update all LEDs (= make LED 10 red)
    delay(15);
  }

  // Verify Panel 1 is lit
  // Begin "monitor flicker"
  strip.show();
  flicker(0, 9, 0, 255, 0, L, laddOnDelay, 50);

  // "Breathe" down from Panel 1
  for (int t1 = L; t1 >= 0; t1--) {
    for (int thisPin = 0; thisPin < 9; thisPin++) {
      setColorAndBrightness(thisPin, 0, 255, 0, t1);
    }
    strip.show();                // Update all LEDs (= make LED 10 red)
    delay(15);
  }
  // Set strip to all "off"
  for (int thisPin = 0; thisPin < strip.numPixels(); thisPin++) {
    setColorAndBrightness(thisPin, 0, 0, 0, L);
  }

  // Reset brightness
  strip.setBrightness(L);

  // "Breathe" Panel 2 up in White for Intro Crawl Test
  for (int t2 = 0; t2 <= L; t2++) {
    for (int thisPin = 9; thisPin < 18; thisPin++) {
      setColorAndBrightness(thisPin, 255, 255, 255, t2);
    }
    strip.show();                // Update all LEDs (= make LED 10 red)
    delay(15);
  }

  // Verify Panel 2 is lit
  // Begin "monitor flicker"
  strip.show();
  flicker(9, 18, 255, 255, 255, L, textOnDelay, 10);

  // "Breathe" Panel 2 down
  for (int t2 = L; t2 >= 0; t2--) {
    for (int thisPin = 9; thisPin < 18; thisPin++) {
      setColorAndBrightness(thisPin, 255, 255, 255, t2);
    }
    strip.show();                // Update all LEDs (= make LED 10 red)
    delay(15);
  }

  // Reset all pixels to Off
  for (int thisPin = 0; thisPin < strip.numPixels(); thisPin++) {
    setColorAndBrightness(thisPin, 0, 0, 0, L);
  }
  strip.show();
}

void setColorAndBrightness(uint16_t n, uint8_t r, uint8_t g, uint8_t b, uint16_t brightness) {
  strip.setPixelColor(n, (brightness * r / 255) , (brightness * g / 255), (brightness * b / 255));
}

void flicker(uint16_t minN, uint16_t maxN, uint8_t r, uint8_t g, uint8_t b, uint16_t brightness, uint8_t wait, uint8_t rate) {

  // Above is old code.
  uint8_t counter = 0;
  uint8_t onWait = 0;
  uint8_t offWait = 0;
  uint8_t halfRate = rate / 2;

  // Loop "flicker" until total wait time is met
  while (counter < wait) {
    // turn on desired pixel range to desired color / brightness
    for (int thisPin = minN; thisPin < maxN; thisPin++) {
      setColorAndBrightness(thisPin, r, g, b, brightness);
    }
    strip.show();
    // wait for random time up to "rate" with pixels on
    // minimum of "half rate"
    onWait = random(halfRate, rate);
    counter += onWait;
    delay(onWait);

    // turn off desired pixel range
    // maximum of half rate
    for (int thisPin = minN; thisPin < maxN; thisPin++) {
      setColorAndBrightness(thisPin, r, g, b, 0);
    }
    strip.show();
    // wait for random time up to rate with pixels off
    offWait = random(0, halfRate);
    counter += offWait;
    delay(offWait);
  }
  // end loop with pixels on
  for (int thisPin = minN; thisPin < maxN; thisPin++) {
    setColorAndBrightness(thisPin, r, g, b, brightness);
  }
  strip.show();
}

// Neopixel library example for "wiping" color across the strip
// Used here to clear...
void colorWipe(uint32_t color, int wait) {
  for (int i = 0; i < 30; i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}
